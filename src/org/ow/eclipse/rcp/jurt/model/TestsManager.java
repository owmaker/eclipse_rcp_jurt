package org.ow.eclipse.rcp.jurt.model;

import java.io.File;
//import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.util.Arrays;
//import java.util.HashSet;
import java.util.*;

//import java.util.Map;
/*import org.eclipse.core.internal.runtime.InternalPlatform;
import org.eclipse.osgi.framework.adaptor.FrameworkAdaptor;
import org.eclipse.osgi.framework.internal.core.BundleContextImpl;
import org.eclipse.osgi.framework.internal.core.Framework;*/
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.wiring.BundleWiring;
import org.ow.eclipse.rcp.jurt.Activator;
import org.ow.eclipse.rcp.jurt.gui.view.View;
import org.ow.eclipse.rcp.jurt.model.ParamsConfig.BundleParams;
import org.ow.eclipse.rcp.jurt.model.ParamsConfig.ClassParams;
import org.ow.eclipse.rcp.jurt.model.wrappers.*;

public class TestsManager {

	private View					view;
	private ArrayList<RowData<?>>	rows	= new ArrayList<>();
	private boolean					testSearch;
	private boolean					testExecution;

	private int						lastRunTests;
	private int						lastSummTests;
	private int						lastErrors;
	private int						lastFailures;

	public TestsManager(View view) {
		this.view = view;
	}

	public View getView() {
		return view;
	}

	//------------------------------
	
	public ArrayList<RowData<?>> getTableRows() {
		return rows;
	}
	
	public ArrayList<RowData<?>> getBundleRows() {
		ArrayList<RowData<?>> bRows = new ArrayList<>();
		for (RowData<?> row : rows) {
			if (row instanceof BundleRow) {
				bRows.add(row);
			}
		}
		return bRows;
	}

	public boolean isTestSearch() {
		return testSearch;
	}

	public boolean isTestExecution() {
		return testExecution;
	}

	//------------------------------

	public int getLastRunTestsCount() {
		return lastRunTests;
	}

	public int getLastSummTestsCount() {
		return lastSummTests;
	}

	public int getLastErrorsCount() {
		return lastErrors;
	}

	public int getLastFailuresCount() {
		return lastFailures;
	}
	
	//------------------------------
	
	public void findTests() {
		testSearch = true;
		rows.clear();
		initRows();
		testSearch = false;
	}

	private void initRows() {

		Bundle[] bs = getBundles1();

		int summClasses = 0;
		int testBundles = 0;
		int testClasses = 0;
		int testMethods = 0;
		for (Bundle bundle : bs) {

			BundleWiring bundleWiring = bundle.adapt(BundleWiring.class);
			if (bundleWiring == null) continue;
			List<URL> resources = bundleWiring.findEntries("/", "*.class", BundleWiring.FINDENTRIES_RECURSE);

			LinkedHashMap<Class<?>, LinkedHashSet<Method>> bundleTestClasses = new LinkedHashMap<Class<?>, LinkedHashSet<Method>>();

			for (URL resource : resources) {
				if (resource != null) {
					String className = resource.getPath().replaceAll("/", ".");

					if (className.startsWith(".")) className = className.substring(1);
					if (className.startsWith("bin.")) className = className.substring(4);			//TODO possible trouble here
					int k1 = className.lastIndexOf(".class");
					if (k1 == className.length() - 6) className = className.substring(0, k1);
					int k2 = className.indexOf("$");
					if (k2 != -1) className = className.substring(0, k2);

					try {
						Class<?> clazz = bundle.loadClass(className);								//TODO possible trouble here
						summClasses++;

						LinkedHashSet<Method> classTestMethods = new LinkedHashSet<Method>();

						Method[] methods = clazz.getMethods();
						for (Method method : methods) {
							if(method.isAnnotationPresent(org.junit.Test.class)
								&& !method.isAnnotationPresent(org.junit.Ignore.class))
									classTestMethods.add(method);
						}

						if (classTestMethods.isEmpty()) continue;
						bundleTestClasses.put(clazz, classTestMethods);

					} catch (Error | Exception e) {
						//e.printStackTrace();
					}
				}
			}

			if (bundleTestClasses.isEmpty()) continue;
			testBundles++;

			BundleRow bRow = new BundleRow(bundle, null, rows);
			rows.add(bRow);
			for (Class<?> clazz : bundleTestClasses.keySet()) {
				ClassRow cRow = new ClassRow(clazz, bRow, rows, this);
				testClasses++;
				int i = 0;
				for (Method method : bundleTestClasses.get(clazz)) {
					new MethodRow(method, cRow, i);
					i++;
					testMethods++;
				}
			}
		}

		String str = 
				"\n----------------------------" + 
				"\nJuRT - " + DateFormat.getDateTimeInstance().format(new Date()) + 
				"\n" + 
				"\nJuRT - Summary bundles = " + bs.length + 
				"\nJuRT - Summary classes = " + summClasses + 
				"\n" + 
				"\nJuRT - Test bundles = " + testBundles + 
				"\nJuRT - Test classes = " + testClasses + 
				"\nJuRT - Test methods = " + testMethods + 
				"\n----------------------------";

		System.out.println(str);
	}

	public void runTests() {
		ArrayList<RowData<?>> bRows = getBundleRows();
		Collections.sort(bRows);
		int i = 0;
		for (RowData<?> row : bRows) {
			i++;
			view.getViewPanel().setProgressbarValue(i, bRows.size());
			if (!row.isChecked()) continue;
			for (RowData<?> childRow : row.getChildren()) {
				if (!childRow.isChecked()) continue;
				if (childRow instanceof ClassRow) {
					((ClassRow) childRow).testClass();
				}
			}
		}
		gatherLastRunSummInfo();
	}
	
	private void gatherLastRunSummInfo(){
		for (RowData<?> row : rows) {
			if (row instanceof MethodRow) {
				MethodRow mRow = (MethodRow) row;
				lastSummTests++;
				if (!row.isChecked()) continue;
				lastRunTests++;
				if(mRow.initState()==FulfillState.error) lastErrors++;
				if(mRow.initState()==FulfillState.failure) lastFailures++;
			}
		}
	}

	public void clear() {
		clearRows();
		clearSummInfo();
	}

	public void clearRows() {
		rows.clear();
	}

	public void clearSummInfo() {
		lastRunTests = 0;
		lastSummTests = 0;
		lastErrors = 0;
		lastFailures = 0;
	}
	
	private static Bundle[] getBundles1() {
		BundleContext ctxt = Activator.getDefault().getBundle().getBundleContext();
		return ctxt.getBundles();
	}

	//------------------------------
	
	public void loadConfig() {
		
		FileDialog dialog = new FileDialog(View.getShell(), SWT.OPEN);
		dialog.setFilterExtensions(new String[]{ "*.xml" });

		String filePath = dialog.open();
		if(filePath == null) return;
		try {
			File file = new File(filePath);
			ParamsConfig config = ParamsConfig.read(file);
			
			for(RowData<?> row : getBundleRows()) {
				BundleRow bRow = (BundleRow) row;
				
				for(BundleParams bundleParams : config.getBundles()) {
					if(!bRow.getName().equals(bundleParams.getBundleId())) continue;
					
					for(RowData<?> bChildRow : bRow.getChildren()) {
						if(bChildRow instanceof ClassRow) {
							ClassRow cRow = (ClassRow) bChildRow;
							
							for(ClassParams classParams : bundleParams.getClasses()) {
								if(!cRow.getName().equals(classParams.getClassName())) continue;
								
								if(cRow.getParameters() != null) {
									cRow.getParameters().setParams(classParams);
								}
								break;
							}
						}
					}
					break;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveConfigForClass(ClassRow classRow) {
		ParamsConfig config = new ParamsConfig();
		
		if(classRow.getParameters() != null && classRow.getParent() instanceof BundleRow) {
			
			BundleRow bRow = (BundleRow) classRow.getParent();
			BundleParams bundleParams = new BundleParams(bRow.getName(), bRow.getVersion());
			bundleParams.addClass(classRow.getParameters().getParams());
			config.addBundle(bundleParams);
			
			FileDialog dialog = new FileDialog(View.getShell(), SWT.SAVE);
			dialog.setFilterExtensions(new String[]{ "*.xml" });
			dialog.setFileName("JuRT_class_config_" + getFormattedCurrDate());
			
			String filePath = dialog.open();
			if(filePath == null) return;
			try {
				File file = new File(filePath);
				ParamsConfig.write(config, file);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void saveConfigForAll() {
		ParamsConfig config = new ParamsConfig();
		
		for(RowData<?> row : getBundleRows()) {
			BundleRow bRow = (BundleRow) row;
			BundleParams bundleParams = new BundleParams(bRow.getName(), bRow.getVersion());
			
			for(RowData<?> bChildRow : bRow.getChildren()) {
				if(bChildRow instanceof ClassRow) {
					ClassRow cRow = (ClassRow) bChildRow;
					
					if(cRow.getParameters() != null) {
						bundleParams.addClass(cRow.getParameters().getParams());
					}
				}
			}
			
			if(bundleParams.getClasses().size() != 0) {
				config.addBundle(bundleParams);
			}
		}
		
		FileDialog dialog = new FileDialog(View.getShell(), SWT.SAVE);
		dialog.setFilterExtensions(new String[]{ "*.xml" });
		dialog.setFileName("JuRT_config_" + getFormattedCurrDate());
		
		String filePath = dialog.open();
		if(filePath == null) return;
		try {
			File file = new File(filePath);
			ParamsConfig.write(config, file);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static String getFormattedCurrDate() {
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy.MM.dd_HH:mm:ss");
		return getLegalFilename(sdf.format(Calendar.getInstance().getTime()));
	}
	
	private static String getLegalFilename(String filename) {
		return filename.replaceAll("[\\/:*?\"<>|]",".");
	}
}
