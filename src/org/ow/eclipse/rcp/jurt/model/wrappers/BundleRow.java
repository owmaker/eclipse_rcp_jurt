package org.ow.eclipse.rcp.jurt.model.wrappers;

import java.util.ArrayList;
import java.util.Enumeration;

import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.model.FulfillState;

public class BundleRow extends RowData<Bundle> {

	private static Image img = SWTResourceManager.getImage(BundleRow.class, "/resources/icons/obj_plugin.png");
	
	private String version;

	public BundleRow(Bundle obj, RowData<?> parent, ArrayList<RowData<?>> rows) {
		super(obj, parent);
		this.name = obj.getSymbolicName();
		this.version = obj.getVersion().toString();
		this.rows = rows;
		if(parent!=null) parent.addChild(this);
		init();
	}
	
	public void init(){
		Enumeration<String> keys = obj.getHeaders().keys();
		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			String value = obj.getHeaders().get(key);
			if(value.indexOf(",")==-1){
				description += key + ": " + value + "\n";
			} else {
				description += key + ":\n";
				String[] valueLines = value.split("(,)");
				for(String valueLine : valueLines){
					description += "\t" + valueLine + "\n";
				}
			}
		}
	}

	//------------------------------
	
	protected void initResTime(){
		lastResTime = 0L;
		for(RowData<?> child : children){
			if(child instanceof ClassRow){
				ClassRow cRow = (ClassRow) child;
				if(cRow.getLastResTime()==null) continue;
				lastResTime += cRow.getLastResTime();
			}
		}
	}

	protected void initResDescription() {
		lastResDescription = "";
		for(RowData<?> child : children){
			if(child instanceof ClassRow){
				ClassRow cRow = (ClassRow) child;
				if(cRow.getLastResState() == FulfillState.error
					|| cRow.getLastResState() == FulfillState.failure)
					lastResDescription += cRow.name + (": \n" + cRow.getLastResDescription()).replace("\n", "\n\t") + "\n\n";
			}
		}
	}

	//------------------------------
	
	public String getVersion() {
		return version;
	}
	
	@Override
	public Image getObjectIcon() {
		return img;
	}

	@Override
	public String toString() {
		return name + "  -  " + version;
	}

	@Override
	public int compareTo(RowData<Bundle> o) {
		return this.getName().compareTo(o.getName());
	}

}
