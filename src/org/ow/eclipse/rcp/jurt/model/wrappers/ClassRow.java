package org.ow.eclipse.rcp.jurt.model.wrappers;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.model.TestClassParams;
import org.ow.eclipse.rcp.jurt.model.TestsManager;

public class ClassRow extends RowData<Class<?>> {

	private static Image			img	= SWTResourceManager.getImage(ClassRow.class, "/resources/icons/obj_class.gif");

	protected TestsManager			testsManager;
	private boolean					testing;
	private Result					lastResult;

	private TestClassParams			parameters;

	public ClassRow(Class<?> obj, RowData<?> parent, ArrayList<RowData<?>> rows, TestsManager testsManager) {
		super(obj, parent);
		this.testsManager = testsManager;
		this.name = obj.getName();
		this.rows = rows;
		if (parent != null) parent.addChild(this);
		init();
	}

	public void init() {
		
		parameters = TestClassParams.getTestClassParams(obj);

		try {
			Method getDescrMethod = obj.getMethod("getDescription", String.class);
			Object desc = getDescrMethod.invoke(null, new Object[] {null});
			if (desc != null && desc instanceof String) description = (String) desc;
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	//------------------------------

	public TestClassParams getParameters() {
		return parameters;
	}

	public boolean isTesting() {
		return testing;
	}

	public Result getLastResult() {
		return lastResult;
	}

	//------------------------------

	public void testClass() {
		testing = true;
		lastResult = null;
		setChildMethodsFailure();
		reinitState();
		testsManager.getView().getViewPanel().redrawTable();

		applyParameters();
		lastResult = JUnitCore.runClasses(getObject());
		testing = false;
		setChildMethodsFailure();
		reinitState();
	}
	
	private void applyParameters(){
		if(parameters == null) return;
		parameters.applyParams();
	}

	private void setChildMethodsFailure() {
		for (RowData<?> child : children) {
			if (child instanceof MethodRow) {
				MethodRow mRow = (MethodRow) child;
				Failure failure = null;
				if (lastResult != null) {
					for (Failure f : lastResult.getFailures()) {
						String failureMethodName = f.getDescription().getMethodName();
						int k = failureMethodName.indexOf("[");
						failureMethodName = (k == -1) ? failureMethodName : failureMethodName.substring(0, k);
						if (f.getDescription().getMethodName() == null || failureMethodName.equals(mRow.name)) {
							failure = f;
							break;
						}
					}
				}
				mRow.setFailure(failure);
			}
		}
	}

	//------------------------------

	protected void initResDescription() {
		lastResDescription = "";
		if (lastResult == null) return;
		
		int lastParamLineIndex = -1;
		for (Failure f : lastResult.getFailures()) {
			String failureMethodName = f.getDescription().getMethodName();
			String shortDescr = f.getException().toString();

			int currParamLineIndex = -1;
			int k1 = failureMethodName.indexOf("[");
			int k2 = failureMethodName.indexOf("]");
			if(k1 != -1 && k2 != -1 && k1 < k2) {
				try {
					currParamLineIndex = Integer.parseInt(failureMethodName.substring(k1 + 1, k2));
				} catch (NumberFormatException e) { }
			}
			if(lastResult.getFailures().size() > 0 && lastParamLineIndex != currParamLineIndex) {
				if(!lastResDescription.isEmpty()) lastResDescription += "\n";
				lastResDescription += currParamLineIndex + " paramLine:\n";
			}
			
			lastResDescription += ((failureMethodName == null) ? "" : failureMethodName + ": ") + shortDescr + "\n";

			lastParamLineIndex = currParamLineIndex;
		}
	}

	protected void initResTime() {
		lastResTime = 0L;
		if (lastResult == null) return;
		lastResTime = lastResult.getRunTime();
	}

	//------------------------------

	@Override
	public Image getObjectIcon() {
		return img;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(RowData<Class<?>> o) {
		return this.getName().compareTo(o.getName());
	}

}
