package org.ow.eclipse.rcp.jurt.model.wrappers;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.ow.eclipse.rcp.jurt.model.FulfillState;

public interface IRow {
	
	//----------------------------------------------
	
	public boolean isChecked();
	
	public String getName();

	public FulfillState getLastResState();
	
	public Long getLastResTime();
	
	public String getLastResDescription();
	
	public Image getObjectIcon();
	
	public Color getBgColor();
	
	public Color getFgColor();
	
	//----------------------------------------------
	
	public Object getParent();
	
	public Object getObject();
	
	//----------------------------------------------
	
	public Object getValueForCol(String columnLabel);
	
	public void reinitState();
	
}
