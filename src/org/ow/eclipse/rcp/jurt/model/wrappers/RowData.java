package org.ow.eclipse.rcp.jurt.model.wrappers;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.model.FulfillState;

public abstract class RowData<T extends Object> implements IRow, Comparable<RowData<T>> {

	public static final String[]	columnNames			= {"check", "name", "state", "time"};
	public static final String[]	columnTitles		= {Character.toString((char) 0x2714), "Name", "State", "Time, s"};

	protected T						obj;
	protected RowData<?>			parent;


	protected boolean				checked				= true;
	protected String				name;
	protected String				description			= "";
	
	protected FulfillState			lastResState;
	protected Long					lastResTime			= null;
	protected String				lastResDescription	= "";

	protected ArrayList<RowData<?>>	children			= new ArrayList<>();
	protected ArrayList<RowData<?>>	rows;

	public RowData(T obj, RowData<?> parent) {
		this.obj = obj;
		this.parent = parent;
		this.lastResState = FulfillState.undefined;
	}

	public Object getValueForCol(int columnIndex) {
		return getValueForCol(columnNames[columnIndex]);
	}

	public Object getValueForCol(String columnLabel) {
		switch (columnLabel) {
		case "check":
			return Boolean.valueOf(checked);
		case "name":
			return toString();
		case "state":
			return null;
		case "time":
			return (lastResTime == null) ? null : ((double) lastResTime / 1000);
		default:
			return null;
		}
	}

	public void setValueForCol(int columnIndex, Object value) {
		setValueForCol(columnNames[columnIndex], value);
	}

	public void setValueForCol(String columnLabel, Object value) {
		switch (columnLabel) {
		case "check":
			checked = Boolean.parseBoolean((String) value);
			break;
		default:
			break;
		}
	}

	//-------------------------------------

	public boolean isChecked() {
		return checked;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public FulfillState getLastResState() {
		if (lastResState == null) reinitState();
		return lastResState;
	}

	public Long getLastResTime() {
		return lastResTime;
	}

	public String getLastResDescription() {
		return lastResDescription;
	}

	//-------------------------------------

	protected abstract void initResTime();

	protected abstract void initResDescription();

	//-------------------------------------

	public abstract Image getObjectIcon();

	public Image getStateIcon() {
		return lastResState.getImage();
	}

	public Color getBgColor() {
		return SWTResourceManager.getColor(SWT.COLOR_WHITE);
	}

	public Color getFgColor() {
		return SWTResourceManager.getColor(SWT.COLOR_BLACK);
	}

	//-------------------------------------

	public T getObject() {
		return obj;
	}

	public RowData<?> getParent() {
		return parent;
	}

	public void setParent(RowData<?> parent) {
		this.parent = parent;
	}

	public ArrayList<RowData<?>> getChildren() {
		return new ArrayList<RowData<?>>(children);
	}

	public void addChild(RowData<?> childRow) {
		if (childRow instanceof ClassRow) {
			ClassRow cRow = (ClassRow) childRow;
			children.add(cRow);
		}
		if (childRow instanceof MethodRow) {
			MethodRow mRow = (MethodRow) childRow;
			int k = rows.indexOf(this);
			k = k + children.size() + 1;
			rows.add(k, mRow);
			children.add(mRow);
		}
	}

	//-------------------------------------

	public void reinitState() {
		lastResState = initState();

		initResTime();
		initResDescription();

		if (parent != null) parent.reinitState();
	}

	public FulfillState initState() {

		boolean childsSummStateIsSuccess = true;
		boolean childsSummStateIsError = true;
		boolean childsSummStateIsFailure = true;
		boolean childsSummStateIsWaiting = true;
		boolean childsSummStateIsUndefined = true;

		for (RowData<?> child : children) {
			FulfillState childState = child.getLastResState();
			childsSummStateIsSuccess = childsSummStateIsSuccess && (childState == FulfillState.success);
			childsSummStateIsError = childsSummStateIsError && (childState == FulfillState.error || childState == FulfillState.success || childState == FulfillState.failure);
			childsSummStateIsFailure = childsSummStateIsFailure && (childState == FulfillState.failure || childState == FulfillState.success);
			childsSummStateIsWaiting = childsSummStateIsWaiting
					&& (childState == FulfillState.waiting || childState == FulfillState.success || childState == FulfillState.failure || childState == FulfillState.undefined);
			childsSummStateIsUndefined = childsSummStateIsUndefined && (childState == FulfillState.undefined);
		}

		if (childsSummStateIsUndefined) return FulfillState.undefined;
		else if (childsSummStateIsSuccess) return FulfillState.success;
		else if (childsSummStateIsFailure) return FulfillState.failure;
		else if (childsSummStateIsError) return FulfillState.error;
		return FulfillState.waiting;
	}

}
