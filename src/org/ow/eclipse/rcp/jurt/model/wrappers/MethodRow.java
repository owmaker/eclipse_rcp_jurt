package org.ow.eclipse.rcp.jurt.model.wrappers;

import java.lang.reflect.Method;

import org.eclipse.swt.graphics.Image;
import org.junit.runner.notification.Failure;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.model.FulfillState;

public class MethodRow extends RowData<Method> {

	private static Image img = SWTResourceManager.getImage(MethodRow.class, "/resources/icons/obj_test.gif");
			
	private int index;
	private Failure failure;

	public MethodRow(Method obj, RowData<?> parent, int index) {
		super(obj, parent);
		this.name = obj.getName();
		this.index = index;
		parent.addChild(this);
		init();
	}
	
	public void init(){
		try {
			if(parent instanceof ClassRow){
				Method getDescrMethod = ((ClassRow) parent).getObject().getMethod("getDescription", String.class);
				Object desc = getDescrMethod.invoke(null, new Object[]{name.substring(name.lastIndexOf(".")+1)});
				if(desc!=null && desc instanceof String) description = (String) desc;
			}
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	//------------------------------
	
	public int getIndex() {
		return index;
	}
	
	void setFailure(Failure failure){
		this.failure = failure;
		reinitState();
	}
	
	//------------------------------

	@Override
	public FulfillState initState() {
		if(failure == null){
			if(getParent() instanceof ClassRow){
				ClassRow cRow = (ClassRow) getParent();
				if(cRow.isTesting()) return FulfillState.waiting;
			}
		}
		return FulfillState.getValue(failure);
	}
	
	protected void initResTime(){
	}

	protected void initResDescription() {
		lastResDescription = (failure==null)? "" : failure.getTrace() ;
	}

	//------------------------------
	
	@Override
	public Image getObjectIcon() {
		return img;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(RowData<Method> o) {
		return Integer.compare(this.getIndex(), ((MethodRow)o).getIndex());
	}
}
