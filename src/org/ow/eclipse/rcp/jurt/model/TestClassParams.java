package org.ow.eclipse.rcp.jurt.model;

import java.lang.reflect.*;
import java.util.*;

import org.ow.eclipse.rcp.jurt.model.ParamsConfig.ClassParams;
import org.ow.eclipse.rcp.jurt.model.ParamsConfig.ParamLine;
import org.ow.eclipse.rcp.jurt.model.ParamsConfig.Parameter;

public class TestClassParams {

	private Class<?>						clazz;
	private ArrayList<String>				paramTitles;
	private ClassParams						params;
	private boolean							editable;

	public static TestClassParams getTestClassParams(Class<?> clazz) {
		if (!isClassParametrized(clazz)) return null;
		return new TestClassParams(clazz);
	}

	private TestClassParams(Class<?> clazz) {
		this.clazz = clazz;
		init();
	}
	
	//--------------------------
	
	public Class<?> getClazz() {
		return clazz;
	}

	public ArrayList<String> getParamTitles() {
		return paramTitles;
	}

	public ClassParams getParams() {
		return params;
	}
	
	public void setParams(ClassParams params) {
		if(!isEditable()) return;
		int l = paramTitles.size();
		boolean applyable = true;
		if(params.getParamLines().size() != 0) {
			for(ParamLine line : params.getParamLines()) {
				if(line.getParams().size() != l) {
					applyable = false;
				}
			}
		}
		if(applyable) this.params = params;
	}
	
	public boolean isEditable() {
		return editable;
	}
	
	//--------------------------
	
	public void applyParams(){
		if(!editable) return;
		writeParams();
	}
	
	//--------------------------

	private void init() {
		paramTitles = new ArrayList<>();
		params = new ClassParams(clazz.getName());
		
		try {
			Method paramsMethod = getParamsMethod(clazz);
			Class<?>[] paramsClasses = getParamsClasses(paramsMethod);
			int i = 0;
			for (Class<?> paramsClass : paramsClasses) {
				String typeName = paramsClass.getTypeName();
				typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
				paramTitles.add("arg" + i + " [" + typeName + "]");
				i++;
			}
			readParams();
		} catch (Exception e) {
			// e.printStackTrace();
		}
		
		editable = (getParamsField(clazz)!= null);
	}

	private static boolean isClassParametrized(Class<?> clazz) {
		boolean isRunWith = (clazz.getAnnotation(org.junit.runner.RunWith.class) != null);
		Method paramsMethod = getParamsMethod(clazz);
		Constructor<?> paramsConstructor = getConstructorByParamsMethod(clazz, paramsMethod);
		return isRunWith && (paramsMethod != null) && (paramsConstructor != null);
	}

	private static Method getParamsMethod(Class<?> clazz) {
		try {
			Method[] methods = clazz.getMethods();
			for (Method method : methods) {
				if (method.isAnnotationPresent(org.junit.runners.Parameterized.Parameters.class)) return method;
			}
		} catch (Error | Exception e) {
			// e.printStackTrace();
		}
		return null;
	}

	private static Constructor<?> getConstructorByParamsMethod(Class<?> clazz, Method paramsMethod) {
		if (paramsMethod == null) return null;
		try {
			Class<?>[] paramsClasses = getParamsClasses(paramsMethod);
			return getConstructor(clazz, paramsClasses);
		} catch (Error | Exception e) {
			// e.printStackTrace();
		}
		return null;
	}

	private static Class<?>[] getParamsClasses(Method paramsMethod) {
		if (paramsMethod == null) return null;
		try {
			Type paramsMethodReturnType = paramsMethod.getGenericReturnType();
			String sampleReturnTypeName = "java.util.Collection<java.lang.Object[]>";

			if (paramsMethodReturnType.getTypeName().equals(sampleReturnTypeName)) {

				Object desc = paramsMethod.invoke(null, new Object[] {});
				Collection<?> coll = (Collection<?>) desc;
				Object params = coll.iterator().next();

				if (params instanceof Object[]) {
					Object[] arr = (Object[]) params;
					Class<?>[] paramsClasses = new Class<?>[arr.length];
					for (int i = 0; i < arr.length; i++) {
						paramsClasses[i] = arr[i].getClass();
					}
					return paramsClasses;
				}
			}
		} catch (Error | Exception e) {
			// e.printStackTrace();
		}
		return null;
	}

	private static Constructor<?> getConstructor(Class<?> clazz, Class<?>[] paramsClasses) {
		if (paramsClasses == null) return null;
		try {
			return clazz.getConstructor(paramsClasses);
		} catch (NoSuchMethodException | SecurityException e) {
			// e.printStackTrace();
		}
		return null;
	}

	private static Field getParamsField(Class<?> clazz){
		try {
			return clazz.getField("TEST_PARAMS");
		} catch (Error | Exception e) {
			// e.printStackTrace();
		}
		return null;
	}

	//--------------------------
	
	private void readParams(){
		try {
			Method paramsMethod = getParamsMethod(clazz);
			Object desc = paramsMethod.invoke(null, new Object[] {});
			Collection<?> coll = (Collection<?>) desc;

			for (Iterator<?> it = coll.iterator(); it.hasNext();) {
				Object mmbr = it.next();
				if (mmbr instanceof Object[]) {
					Object[] arr = (Object[]) mmbr;
					ParamLine paramLine = new ParamLine();
					for (Object val : arr) {
						String typeName = val.getClass().getTypeName();
						typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
						paramLine.addParam(new Parameter(val.toString()));
					}
					params.addParamLine(paramLine);
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	private void writeParams(){
		try {
			Collection<Object[]> coll = params.getRawValues();
			Field paramsField = getParamsField(clazz);
			paramsField.set(null, coll);
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}
	
	
}
