package org.ow.eclipse.rcp.jurt.model;

import org.eclipse.swt.graphics.Image;
import org.junit.ComparisonFailure;
import org.junit.runner.notification.Failure;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.model.wrappers.BundleRow;

public enum FulfillState {
	
	success,
	error,
	failure,
	waiting,
	undefined;
	
	private static Image success_img = SWTResourceManager.getImage(BundleRow.class, "/resources/icons/state_success.png");
	private static Image error_img = SWTResourceManager.getImage(BundleRow.class, "/resources/icons/state_error.png");
	private static Image failure_img = SWTResourceManager.getImage(BundleRow.class, "/resources/icons/state_failed.png");
	private static Image waiting_img = SWTResourceManager.getImage(BundleRow.class, "/resources/icons/state_waiting.png");
	private static Image undefined_img = SWTResourceManager.getImage(BundleRow.class, "/resources/icons/state_undefined.png");
	
	public String getDisplayable(){
		switch (this) {
			case success: return "Success";//"Выполнено";
			case error: return "Error";//"Ошибка";
			case failure: return "Failure";//"Отказ";
			case waiting: return "Waiting";//"Ожидание";
			default: return "Undefined";//"Не определено";
		}
	}
	
	public static String[] getDisplayableValues(){
		int l = values().length;
		String[] values = new String[l];
		for(int i = 0 ; i < l ; i++){
			values[i] = values()[i].getDisplayable();
		}
		return values;
	}
	
	public Image getImage(){
		switch (this) {
			case success: return success_img;
			case error: return error_img;
			case failure: return failure_img;
			case waiting: return waiting_img;
			default: return undefined_img;
		}
	}
	
	public static FulfillState getValue(String displayable){
		for(FulfillState value : FulfillState.values()){
			if(value.getDisplayable().equals(displayable)) return value;
		}
		return undefined;
	}
	
	public static FulfillState getValue(Failure fail){
		if(fail==null) return success;
		Throwable failureException = fail.getException();
		if(AssertionError.class.isInstance(failureException)
			|| ComparisonFailure.class.isInstance(failureException)) return failure;
		return error;
	}
	
}
