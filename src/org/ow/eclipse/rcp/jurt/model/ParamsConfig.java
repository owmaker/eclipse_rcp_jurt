package org.ow.eclipse.rcp.jurt.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.*;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "config")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParamsConfig {

	@XmlElement(name = "bundle", type = BundleParams.class)
	private ArrayList<BundleParams> bundles = new ArrayList<>();
	
	public ArrayList<BundleParams> getBundles() {
		return new ArrayList<>(bundles);
	}
	
	public void addBundle(BundleParams bundleParams) {
		bundles.add(bundleParams);
	}

	@XmlRootElement(name = "bundle")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class BundleParams {
		
		@XmlAttribute(name = "id")
		private String id;
		
		@XmlAttribute(name = "ver")
		private String version;

		@XmlElement(name = "class", type = ClassParams.class)
		private ArrayList<ClassParams> classes = new ArrayList<>();
		
		public BundleParams() {};
		
		public BundleParams(String id, String version) {
			this.id = id;
			this.version = version;
		};
		
		public String getBundleId() {
			return id;
		}
		
		public String getBundleVersion() {
			return version;
		}
		
		public ArrayList<ClassParams> getClasses() {
			return new ArrayList<>(classes);
		}
		
		public void addClass(ClassParams classParams) {
			classes.add(classParams);
		}
	}

	@XmlRootElement(name = "class")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class ClassParams {
		
		@XmlAttribute(name = "name")
		private String name;

		@XmlElement(name = "paramLine", type = ParamLine.class)
		private ArrayList<ParamLine> paramLines = new ArrayList<>();
		
		public ClassParams() {};
		
		public ClassParams(String name) {
			this.name = name;
		};
		
		public String getClassName() {
			return name;
		}
		
		public ArrayList<ParamLine> getParamLines() {
			return new ArrayList<>(paramLines);
		}
		
		public ParamLine getParamLine(int index) {
			try {
				return paramLines.get(index);
			} catch (Exception e) {
				return null;
			}
		}
		
		public void addParamLine(ParamLine line){
			paramLines.add(line);
		}
		
		public void removeParamLine(int index){
			paramLines.remove(index);
		}
		
		public Collection<Object[]> getRawValues(){
			Collection<Object[]> list = new ArrayList<>();
			for(ParamLine paramLine : paramLines){
				list.add(paramLine.getRawValues());
			}
			return list;
		}
	}

	@XmlRootElement(name = "paramLine")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class ParamLine {

		@XmlElement(name = "param", type = Parameter.class)
		private ArrayList<Parameter> params = new ArrayList<>();
		
		public ParamLine() {};
		
		public ArrayList<Parameter> getParams() {
			return new ArrayList<>(params);
		}
		
		public Parameter getParam(int index){
			try {
				return params.get(index);
			} catch (Exception e) {
				return null;
			}
		}
		
		public void addParam(Parameter param){
			params.add(param);
		}
		
		public Object[] getRawValues(){
			Object[] arr = new Object[params.size()];
			for(int i = 0 ; i < params.size() ; i++){
				arr[i] = params.get(i).getValue();
			}
			return arr;
		}
	}

	@XmlRootElement(name = "param")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Parameter {
		
		@XmlValue
		private String value;
		
		public Parameter() {};
		
		public Parameter(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return getValue();
		}
	}
	
	public static ParamsConfig read(File file) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ParamsConfig.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		return (ParamsConfig) unmarshaller.unmarshal(file);
	}
	
	public static void write(ParamsConfig paramsConfig, File file) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ParamsConfig.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.marshal(paramsConfig, file);
	}
	
}
