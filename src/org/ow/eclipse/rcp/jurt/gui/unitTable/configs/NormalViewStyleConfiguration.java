package org.ow.eclipse.rcp.jurt.gui.unitTable.configs;

import org.eclipse.nebula.widgets.nattable.config.DefaultNatTableStyleConfiguration;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

public class NormalViewStyleConfiguration extends DefaultNatTableStyleConfiguration {

	private Font initFont;
	
	public NormalViewStyleConfiguration() {
		super();
		font = Display.getCurrent().getSystemFont();
		initFont = font;
	}

	public Font getFont(){
		return initFont;
	}
}
