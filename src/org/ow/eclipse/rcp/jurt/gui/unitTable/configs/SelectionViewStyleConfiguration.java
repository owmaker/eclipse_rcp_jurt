package org.ow.eclipse.rcp.jurt.gui.unitTable.configs;

import org.eclipse.nebula.widgets.nattable.selection.config.DefaultSelectionStyleConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;

public class SelectionViewStyleConfiguration extends DefaultSelectionStyleConfiguration {
	
	private Font initFont;

	public SelectionViewStyleConfiguration(NormalViewStyleConfiguration configuration) {
		super();
		anchorBgColor = SWTResourceManager.getColor(205, 232, 255);
		anchorFgColor = SWTResourceManager.getColor(SWT.COLOR_BLACK);
		selectionFont = configuration.getFont();
		initFont = selectionFont;
	}

	public Font getFont(){
		return initFont;
	}
}
