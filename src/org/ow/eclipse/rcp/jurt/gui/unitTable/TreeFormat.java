package org.ow.eclipse.rcp.jurt.gui.unitTable;

import java.util.Comparator;
import java.util.List;

import org.ow.eclipse.rcp.jurt.model.wrappers.MethodRow;
import org.ow.eclipse.rcp.jurt.model.wrappers.RowData;

import ca.odell.glazedlists.TreeList;

public class TreeFormat<T extends RowData<?>> implements TreeList.Format<T>{

	@Override
	public boolean allowsChildren(T element) {
		if(element instanceof MethodRow) return false;
		return true;
	}

	@Override
	public Comparator<? super T> getComparator(int depth) {
		return new Comparator<T>() {
			@Override
			public int compare(T first, T second) {
				return first.compareTo(second);
			}
		};
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getPath(List<T> path, T element) {
		RowData<?> e = element;
		do{
			path.add(0, (T) e);
			e = e.getParent();
		} while(e != null);
		
	}

}
