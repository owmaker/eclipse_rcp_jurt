package org.ow.eclipse.rcp.jurt.gui.unitTable;

import java.util.*;

import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.command.StructuralRefreshCommand;
import org.eclipse.nebula.widgets.nattable.command.VisualRefreshCommand;
import org.eclipse.nebula.widgets.nattable.config.*;
import org.eclipse.nebula.widgets.nattable.coordinate.PixelCoordinate;
import org.eclipse.nebula.widgets.nattable.coordinate.Range;
import org.eclipse.nebula.widgets.nattable.data.*;
import org.eclipse.nebula.widgets.nattable.edit.EditConfigAttributes;
import org.eclipse.nebula.widgets.nattable.edit.command.EditCellCommandHandler;
import org.eclipse.nebula.widgets.nattable.edit.config.DefaultEditBindings;
import org.eclipse.nebula.widgets.nattable.edit.editor.CheckBoxCellEditor;
import org.eclipse.nebula.widgets.nattable.edit.editor.ICellEditor;
import org.eclipse.nebula.widgets.nattable.edit.event.InlineCellEditEventHandler;
import org.eclipse.nebula.widgets.nattable.extension.glazedlists.GlazedListsEventLayer;
import org.eclipse.nebula.widgets.nattable.extension.glazedlists.filterrow.DefaultGlazedListsStaticFilterStrategy;
import org.eclipse.nebula.widgets.nattable.extension.glazedlists.tree.GlazedListTreeData;
import org.eclipse.nebula.widgets.nattable.extension.glazedlists.tree.GlazedListTreeRowModel;
import org.eclipse.nebula.widgets.nattable.filterrow.FilterRowHeaderComposite;
import org.eclipse.nebula.widgets.nattable.filterrow.FilterRowTextCellEditor;
import org.eclipse.nebula.widgets.nattable.freeze.CompositeFreezeLayer;
import org.eclipse.nebula.widgets.nattable.freeze.FreezeLayer;
import org.eclipse.nebula.widgets.nattable.grid.GridRegion;
import org.eclipse.nebula.widgets.nattable.grid.data.DefaultCornerDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.layer.*;
import org.eclipse.nebula.widgets.nattable.hideshow.ColumnHideShowLayer;
import org.eclipse.nebula.widgets.nattable.layer.*;
import org.eclipse.nebula.widgets.nattable.layer.cell.*;
import org.eclipse.nebula.widgets.nattable.layer.event.ILayerEvent;
import org.eclipse.nebula.widgets.nattable.layer.event.RowStructuralRefreshEvent;
import org.eclipse.nebula.widgets.nattable.painter.cell.CheckBoxPainter;
import org.eclipse.nebula.widgets.nattable.painter.layer.GridLineCellLayerPainter;
import org.eclipse.nebula.widgets.nattable.selection.SelectionLayer;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.nebula.widgets.nattable.tooltip.NatTableContentTooltip;
import org.eclipse.nebula.widgets.nattable.tree.ITreeRowModel;
import org.eclipse.nebula.widgets.nattable.tree.TreeLayer;
import org.eclipse.nebula.widgets.nattable.tree.painter.IndentedTreeImagePainter;
import org.eclipse.nebula.widgets.nattable.tree.painter.TreeImagePainter;
import org.eclipse.nebula.widgets.nattable.viewport.ViewportLayer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.ow.eclipse.rcp.jurt.gui.panels.IDataSelectionListener;
import org.ow.eclipse.rcp.jurt.gui.unitTable.configs.*;
import org.ow.eclipse.rcp.jurt.gui.view.View;
import org.ow.eclipse.rcp.jurt.model.wrappers.MethodRow;
import org.ow.eclipse.rcp.jurt.model.wrappers.RowData;

import ca.odell.glazedlists.*;

public class UnitTableModel {

	@SuppressWarnings("unused")
	private View view;
	private NatTable table;
	private List<RowData<?>> list;
	private String[] columnNames = RowData.columnNames;
	private String[] columnTitles = RowData.columnTitles;
	private boolean showRowHeaders;
	private HashMap<Integer, ICellEditor> columnFilterEditors;
	
	private TableColumnAccessor columnAccessor;
	private BodyLayerStack bodyLayerStack;
	
	private DefaultGlazedListsStaticFilterStrategy<RowData<?>> filterStrategy;
	private FilterRowHeaderComposite<RowData<?>> filterRowHeaderLayer;
	
	private IDataProvider columnHeaderDataProvider;
	private ColumnHeaderLayerStack columnHeaderLayerStack;

	private IDataProvider rowHeaderDataProvider;
	private RowHeaderLayerStack rowHeaderLayerStack;

	private IDataProvider cornerDataProvider;
	private CornerLayer cornerLayer;
	
	private GridLayer gridLayer;
	
	private ConfigRegistry configRegistry;

	public UnitTableModel(View view, NatTable table, List<RowData<?>> data){
		this(view, table, data, true);
	}

	public UnitTableModel(View view, NatTable table, List<RowData<?>> data, boolean showRowHeaders){
		this.view = view;
		this.table = table;
		this.list = data;
		this.showRowHeaders = showRowHeaders;
		initModel();
	}

	//=================================================
	
	private void initModel(){
		table.setConfigRegistry(configRegistry = new ConfigRegistry());
		columnFilterEditors = new HashMap<Integer, ICellEditor>();

		columnAccessor = new TableColumnAccessor(columnNames);
		bodyLayerStack = new BodyLayerStack(list, columnAccessor, new TreeFormat<RowData<?>>());

		columnHeaderDataProvider = new TableColumnHeaderDataProvider(columnTitles);
		columnHeaderLayerStack = new ColumnHeaderLayerStack(columnHeaderDataProvider, bodyLayerStack);

		filterStrategy = new DefaultGlazedListsStaticFilterStrategy<RowData<?>>(bodyLayerStack.getFilterList(), columnAccessor, configRegistry);
		filterRowHeaderLayer = new FilterRowHeaderComposite<RowData<?>>(filterStrategy, columnHeaderLayerStack, columnHeaderDataProvider, configRegistry);
		
		rowHeaderDataProvider = new TableRowHeaderDataProvider(bodyLayerStack.getBodyDataProvider(), showRowHeaders);
		rowHeaderLayerStack = new RowHeaderLayerStack(rowHeaderDataProvider, bodyLayerStack);

		cornerDataProvider = new DefaultCornerDataProvider(columnHeaderDataProvider, rowHeaderDataProvider);
		cornerLayer = new CornerLayer( new DataLayer(cornerDataProvider), rowHeaderLayerStack, filterRowHeaderLayer);
		
		gridLayer = new GridLayer(bodyLayerStack, filterRowHeaderLayer, rowHeaderLayerStack, cornerLayer, false);
		
		table.setLayer(gridLayer);
		initConfigs();
	}
	
	private void initConfigs(){
		initLabels();
		
		initEditConfig();
		initStyleConfig();
		//initFiltersConfig();
		
		//table.addConfiguration(new MenuConfiguration(view, this));
		
		new NatTableContentTooltip(table, GridRegion.BODY);
		new NatTableContentTooltip(table, GridRegion.COLUMN_HEADER);

		table.configure();
	}
	
	private void initLabels(){
		AggregateConfigLabelAccumulator acfa = (AggregateConfigLabelAccumulator) bodyLayerStack.getBodyDataLayer().getConfigLabelAccumulator();
		acfa.add(new IConfigLabelAccumulator() {
			@Override
			public void accumulateConfigLabels(LabelStack configLabels, int columnIndex, int rowIndex) {
				configLabels.addLabel("col_" + columnIndex + "_row_" + rowIndex);
				configLabels.addLabel("col_" + columnIndex);
				configLabels.addLabel("row_" + rowIndex);
			}
		});
	}
	
	private void initEditConfig(){
		gridLayer.addConfiguration(new DefaultEditBindings());
		gridLayer.registerCommandHandler(new EditCellCommandHandler());
		gridLayer.registerEventHandler(new InlineCellEditEventHandler(gridLayer));
		
		configRegistry.registerConfigAttribute(
			EditConfigAttributes.CELL_EDITABLE_RULE,
			IEditableRule.ALWAYS_EDITABLE,
			DisplayMode.EDIT,
			"col_0");
		configRegistry.registerConfigAttribute(
			EditConfigAttributes.CELL_EDITOR, 
			new CheckBoxCellEditor(), 
			DisplayMode.EDIT,
			"col_0");
		configRegistry.registerConfigAttribute(
			CellConfigAttributes.CELL_PAINTER,
			new CheckBoxPainter(),
			DisplayMode.NORMAL,
			"col_0");
	}
	
	private void initStyleConfig(){
		CustomStyleConfiguration csc = new CustomStyleConfiguration(table, this);
		NormalViewStyleConfiguration nvsc = new NormalViewStyleConfiguration();
		SelectionViewStyleConfiguration svsc = new SelectionViewStyleConfiguration(nvsc);
		table.addConfiguration(csc);
		table.addConfiguration(nvsc);
		table.addConfiguration(svsc);
	}
	
	/*private void initFiltersConfig(){
		table.addConfiguration(new FilterRowConfiguration(columnFilterEditors));
		filterRowHeaderLayer.setFilterRowVisible(false);
		
		int l = columnNames.length;
		for(int i = 0 ; i < l ; i++){
			//TODO добавить функционал фильтров столбцов таблицы
			//if(i >= Config.predefTableColumnTitles.length) setColumnComboFilter(i);
		}
	}*/
	
	//==================================================================================================
	
	public class TableColumnAccessor implements IColumnPropertyAccessor<RowData<?>> {
		private String[] columnNames;
		public TableColumnAccessor(String[] columnNames) {
			this.columnNames = columnNames;
		}
		@Override
		public int getColumnCount() {
			return columnNames.length;
		}
		@Override
		public Object getDataValue(RowData<?> object, int columnIndex) {
			return object.getValueForCol(columnIndex);
		}
		@Override
		public void setDataValue(RowData<?> object, int columnIndex, Object newValue) {
			setDataValue(object, columnIndex, newValue, false);
		}
		public void setDataValue(RowData<?> object, int columnIndex, Object newValue, boolean internal) {
			if(object instanceof MethodRow && !internal) return;
			object.setValueForCol(columnIndex, newValue);
			if(columnNames[columnIndex].equals("check")){
				for(RowData<?> child : object.getChildren()){
					setDataValue(child, columnIndex, newValue, true);
				}
			}
		}
		@Override
		public int getColumnIndex(String property) {
			for(int i = 0 ; i < getColumnCount() ; i++){
				if(columnNames[i].equals(property)) return i;
			}
			return 0;
		}
		@Override
		public String getColumnProperty(int columnIndex) {
			return columnNames[columnIndex];
		}
	}
	
	//==================================================================================================
	
	public class BodyLayerStack extends AbstractLayerTransform{

		private List<RowData<?>> values;
		private EventList<RowData<?>> eventList;
		private TransformedList<RowData<?>, RowData<?>> rowObjectsGlazedList;
		private SortedList<RowData<?>> sortedList;
		private FilterList<RowData<?>> filterList;
		private TreeList<RowData<?>> treeList;
		private ListDataProvider<RowData<?>> bodyDataProvider;
		private DataLayer bodyDataLayer;
		private AggregateConfigLabelAccumulator labelAccum;
		private GlazedListsEventLayer<RowData<?>> glazedListsEventLayer;
		private ColumnHideShowLayer columnHideShowLayer;
		private GlazedListTreeData<RowData<?>> treeData;
		private ITreeRowModel<RowData<?>> treeRowModel;
		private SelectionLayer selectionLayer;
		private TreeLayer treeLayer;
		private ViewportLayer viewportLayer;
		private FreezeLayer freeze;
		private CompositeFreezeLayer compFreeze;

		@SuppressWarnings("unchecked")
		public BodyLayerStack(List<RowData<?>> values, IColumnAccessor<RowData<?>> columnAccessor, TreeList.Format<RowData<?>> treeFormat) {
			this.values = values;
			labelAccum = new AggregateConfigLabelAccumulator();
			
			initEventsList();
			initLists();
			
            treeList = new TreeList<RowData<?>>(filterList, treeFormat, TreeList.NODES_START_EXPANDED);
            
			bodyDataProvider = new ListDataProvider<RowData<?>>(treeList, columnAccessor);
			bodyDataLayer = new DataLayer(bodyDataProvider, 100, DataLayer.DEFAULT_ROW_HEIGHT);
			bodyDataLayer.setConfigLabelAccumulator(labelAccum);
			
			glazedListsEventLayer = new GlazedListsEventLayer<RowData<?>>(bodyDataLayer, treeList);
			columnHideShowLayer = new ColumnHideShowLayer(glazedListsEventLayer);
			selectionLayer = new SelectionLayer(columnHideShowLayer);
			selectionLayer.setLayerPainter(new GridLinesPainter());
			
			treeData = new GlazedListTreeData<RowData<?>>(treeList);
            treeRowModel = new GlazedListTreeRowModel<RowData<?>>(treeData);
			treeLayer = new TableTreeLayer(selectionLayer, treeRowModel, new IndentedTreeImagePainter(15, new TreePainter()));
			
			viewportLayer = new ViewportLayer(treeLayer){
				@Override
				public IUniqueIndexLayer getScrollableLayer() {																					//nattable 1.1.0 untibug
					return selectionLayer;
				}
			};
			freeze = new FreezeLayer(treeLayer);
			compFreeze = new CompositeFreezeLayer(freeze, viewportLayer, selectionLayer, false);
			compFreeze.setChildLayer("FROZEN_COLUMN_REGION", new DimensionallyDependentIndexLayer(treeLayer, freeze, viewportLayer), 0, 1);		//nattable 1.1.0 untibug
			
			setUnderlyingLayer(compFreeze);

			glazedListsEventLayer.addLayerListener(new ILayerListener() {
				@Override
				public void handleLayerEvent(ILayerEvent e) {
					if(e instanceof RowStructuralRefreshEvent){
						table.configure();
						table.doCommand(new VisualRefreshCommand());
					}
				}
			});
		}
		
		public void initEventsList(){
			if(eventList == null) eventList = GlazedLists.eventList(values);
			else{
				eventList.clear();
				eventList.addAll(values);
			}
		}
		private void initLists(){
			if(rowObjectsGlazedList == null) rowObjectsGlazedList = GlazedLists.threadSafeList(eventList);
        	if(sortedList == null)  sortedList = new SortedList<RowData<?>>(rowObjectsGlazedList, null);
        	if(filterList == null) filterList = new FilterList<RowData<?>>(sortedList);
		}

		public EventList<RowData<?>> getEventList() {
			return eventList;
		}
		public SortedList<RowData<?>> getSortedList() {
			return sortedList;
		}
		public FilterList<RowData<?>> getFilterList() {
			return filterList;
		}
		public GlazedListsEventLayer<RowData<?>> getGlazedListsEventLayer() {
			return glazedListsEventLayer;
		}
		public ListDataProvider<RowData<?>> getBodyDataProvider() {
			return bodyDataProvider;
		}
		public DataLayer getBodyDataLayer() {
			return bodyDataLayer;
		}
		public SelectionLayer getSelectionLayer() {
			return selectionLayer;
		}
		public TreeLayer getTreeLayer() {
			return treeLayer;
		}
		public ITreeRowModel<RowData<?>> getTreeRowModel() {
			return treeRowModel;
		}
		public ViewportLayer getViewportLayer() {
			return viewportLayer;
		}
		public FreezeLayer getFreeze() {
			return freeze;
		}
		public CompositeFreezeLayer getCompFreeze() {
			return compFreeze;
		}

		public void setColumnWidths(Integer[] widths){
			if(widths!=null){
				for(int i = 0 ; i < widths.length ; i++) bodyDataLayer.setColumnWidthByPosition(i, widths[i]);
			}
		}
	}

	public class ColumnHeaderLayerStack extends AbstractLayerTransform{
		private ColumnHeaderLayer columnHeaderLayer;
		public ColumnHeaderLayerStack(IDataProvider dataProvider, BodyLayerStack bodyLayerStack) {
			DataLayer columnHeaderDataLayer = new DataLayer(dataProvider, 100, DataLayer.DEFAULT_ROW_HEIGHT);
			columnHeaderLayer = new ColumnHeaderLayer( columnHeaderDataLayer, bodyLayerStack, bodyLayerStack.getSelectionLayer(), true);
			setUnderlyingLayer(columnHeaderLayer);
		}
		public ColumnHeaderLayer getColumnHeaderLayer() {
			return columnHeaderLayer;
		}
	}

	public class RowHeaderLayerStack extends AbstractLayerTransform{
		public RowHeaderLayerStack(IDataProvider dataProvider, BodyLayerStack bodyLayerStack) {
			DataLayer rowHeaderDataLayer = new DataLayer(dataProvider, 40, DataLayer.DEFAULT_ROW_HEIGHT);
			RowHeaderLayer rowHeaderLayer = new RowHeaderLayer( rowHeaderDataLayer, bodyLayerStack, bodyLayerStack.getSelectionLayer(), true);
			setUnderlyingLayer(rowHeaderLayer);
		}
	}
	
	//==================================================================================================

	public class TableColumnHeaderDataProvider implements IDataProvider {
		private String[] columnTitles;
		public TableColumnHeaderDataProvider(String[] titles){
			this.columnTitles = titles;
		}
		@Override
		public Object getDataValue(int colIndex, int rowIndex) {
			return columnTitles[colIndex];
		}
		@Override
		public void setDataValue(int colIndex, int rowIndex, Object value) {
			throw new UnsupportedOperationException();
		}
		@Override
		public int getColumnCount() {
			return columnTitles.length;
		}
		@Override
		public int getRowCount() {
			return 1;
		}
	}
	
	public class TableRowHeaderDataProvider implements IDataProvider {
		private IDataProvider bodyDataProvider;
		private boolean showRowHeaders;
		public TableRowHeaderDataProvider(IDataProvider bodyDataProvider, boolean showRowHeaders){
			this.bodyDataProvider = bodyDataProvider;
			this.showRowHeaders = showRowHeaders;
		}
		@Override
		public Object getDataValue(int colIndex, int rowIndex) {
			return rowIndex+1;
		}
		@Override
		public void setDataValue(int colIndex, int rowIndex, Object value) {
			throw new UnsupportedOperationException();
		}
		@Override
		public int getColumnCount() {
			return showRowHeaders ? 1 : 0;
		}
		@Override
		public int getRowCount() {
			return bodyDataProvider.getRowCount();
		}
	}
	
	//==================================================================================================

	public class GridLinesPainter extends GridLineCellLayerPainter{
		@Override
		protected void drawGridLines(ILayer natLayer, GC gc, Rectangle rectangle, IConfigRegistry configRegistry, List<String> labels) {
			Color gColor = (Color)configRegistry.getConfigAttribute(
				CellConfigAttributes.GRID_LINE_COLOR, "NORMAL", new String[0]);
			gc.setForeground(gColor != null ? gColor : getGridColor());
			drawVerticalLines(natLayer, gc, rectangle);
		}
		private void drawVerticalLines(ILayer natLayer, GC gc, Rectangle rectangle) {
			int endY = rectangle.y + Math.min(natLayer.getHeight() - 1, rectangle.height);
			if (endY > natLayer.getHeight()) { return;
			}
			int columnPositionByX = natLayer.getColumnPositionByX(rectangle.x + rectangle.width);
			int maxColumnPosition = columnPositionByX > 0 ? Math.min(natLayer.getColumnCount(), columnPositionByX) : natLayer.getColumnCount();
			for (int columnPosition = natLayer.getColumnPositionByX(rectangle.x); columnPosition < maxColumnPosition; columnPosition++) {
				int size = natLayer.getColumnWidthByPosition(columnPosition);
				if (size > 0) {
					int x = natLayer.getStartXOfColumnPosition(columnPosition) + size - 1;
					gc.drawLine(x, rectangle.y, x, endY);
				}
			}
		}
	}
	
	public class TableTreeLayer extends TreeLayer{
		public TableTreeLayer(IUniqueIndexLayer underlyingLayer, ITreeRowModel<?> treeRowModel, IndentedTreeImagePainter indentedTreeImagePainter) {
			super(underlyingLayer, treeRowModel, indentedTreeImagePainter);
		}
		@Override
		public LabelStack getConfigLabelsByPosition(int columnPosition, int rowPosition) {
			// --- super.super workaround
			int underlyingColumnPosition = localToUnderlyingColumnPosition(columnPosition);
			int underlyingRowPosition = localToUnderlyingRowPosition(rowPosition);
			LabelStack configLabels = underlyingLayer.getConfigLabelsByPosition(underlyingColumnPosition, underlyingRowPosition);
			IConfigLabelAccumulator configLabelAccumulator = getConfigLabelAccumulator();
			if (configLabelAccumulator != null) {
				configLabelAccumulator.accumulateConfigLabels(configLabels, columnPosition, rowPosition);
			}
			String regionName = getRegionName();
			if (regionName != null) {
				configLabels.addLabel(regionName);
			}
			// ---
			if (columnNames[columnPosition].equals("name")) {
				configLabels.addLabelOnTop("TREE_COLUMN_CELL");
				int rowIndex = getRowIndexByPosition(rowPosition);
				configLabels.addLabelOnTop("TREE_DEPTH_" + 
				getModel().depth(rowIndex));
				if (!getModel().hasChildren(rowIndex)) {
					configLabels.addLabelOnTop("TREE_LEAF");
				}
				else if (getModel().isCollapsed(rowIndex)) {
					configLabels.addLabelOnTop("TREE_COLLAPSED");
				}
				else {
					configLabels.addLabelOnTop("TREE_EXPANDED");
				}
			}
			return configLabels;
		}
	}
	
	public class TreePainter extends TreeImagePainter{
		private Image collapsedImage = new Image(null, this.getClass().getResourceAsStream("/resources/icons/plus.png"));
		//private Image expandedImage = new Image(null, this.getClass().getResourceAsStream("/resources/icons/minus.png"));
		private Image leafImage = new Image(null, this.getClass().getResourceAsStream("/resources/icons/leaf.png"));
		private Image collapsedLastImage = new Image(null, this.getClass().getResourceAsStream("/resources/icons/plus_last.png"));
		private Image expandedLastImage = new Image(null, this.getClass().getResourceAsStream("/resources/icons/minus_last.png"));
		private Image leafLastImage = new Image(null, this.getClass().getResourceAsStream("/resources/icons/leaf_last.png"));
		@Override
		protected Image getImage(ILayerCell cell, IConfigRegistry configRegistry) {
			Image icon = null;
			boolean last = isLastCellInGroup(cell);
			if (isLeaf(cell)) {
				icon = (last)? leafLastImage : leafImage ;
			} else if (isCollapsed(cell)) {
				icon = (last)? collapsedLastImage : collapsedImage ;
			} else if (isExpanded(cell)) {
				icon = expandedLastImage;
			}
			return icon;
		}
		private boolean isLeaf(ILayerCell cell) {
			return cell.getConfigLabels().hasLabel("TREE_LEAF");
		}

		private boolean isCollapsed(ILayerCell cell) {
			return cell.getConfigLabels().hasLabel("TREE_COLLAPSED");
		}

		private boolean isExpanded(ILayerCell cell) {
			return cell.getConfigLabels().hasLabel("TREE_EXPANDED");
		}
		
		private boolean isLastCellInGroup(ILayerCell cell){
			int i = cell.getRowIndex();
			if(i == bodyLayerStack.getBodyDataLayer().getRowCount()-1){
				return true;
			} else {
				int depth = bodyLayerStack.getTreeRowModel().depth(i);
				int nextDepth = bodyLayerStack.getTreeRowModel().depth(i+1);
				return nextDepth < depth;
			}
		}
	}

	//==================================================================================================
	
	public String[] getColumnTitles() {
		return columnTitles;
	}

	public void setColumnTitles(String[] columnTitles) {
		this.columnTitles = columnTitles;
	}

	public IColumnAccessor<RowData<?>> getPropertyAccessor() {
		return columnAccessor;
	}

	public ListDataProvider<RowData<?>> getBodyDataProvider() {
		return bodyLayerStack.getBodyDataProvider();
	}

	public DataLayer getBodyDataLayer() {
		return bodyLayerStack.getBodyDataLayer();
	}

	public SelectionLayer getSelectionLayer() {
		return bodyLayerStack.getSelectionLayer();
	}

	public ViewportLayer getViewportLayer() {
		return bodyLayerStack.getViewportLayer();
	}

	public IDataProvider getColumnHeaderDataProvider() {
		return columnHeaderDataProvider;
	}

	public IDataProvider getRowHeaderDataProvider() {
		return rowHeaderDataProvider;
	}

	public IDataProvider getCornerDataProvider() {
		return cornerDataProvider;
	}

	public CornerLayer getCornerLayer() {
		return cornerLayer;
	}

	public GridLayer getGridLayer() {
		return gridLayer;
	}
	
	public void refreshTable(){
		PixelCoordinate origin = bodyLayerStack.getViewportLayer().getOrigin();
		
		bodyLayerStack.initEventsList();
		table.doCommand(new StructuralRefreshCommand());
		table.configure();

		bodyLayerStack.getViewportLayer().setOriginX(origin.getX());
		bodyLayerStack.getViewportLayer().setOriginY(origin.getY());
	}
	
	public void redrawTable(){
		table.doCommand(new VisualRefreshCommand());
		table.configure();
	}

	public void setColumnWidths(Integer[] columnWidths) {
		bodyLayerStack.setColumnWidths(columnWidths);
	}
	
	public void setFiltersVisible(boolean value){
		filterRowHeaderLayer.setFilterRowVisible(value);
	}
	
	public void setColumnTextFilter(int columnIndex){
		columnFilterEditors.put(columnIndex, new FilterRowTextCellEditor());
		table.configure();
	}
	
	/*public void setColumnComboFilter(int columnIndex){
		ComboBoxCellEditor editor = new ComboBoxCellEditor(new IComboBoxDataProvider(){
			public List<?> getValues(int columnIndex, int rowIndex) {
				String key = columnNames[columnIndex];
				key = Config.containsSetting(key)? Config.getSetting(key) : key ;
				//TODO добавить функционал фильтров столбцов таблицы
				//view.getDataManager().getValuesOfDMRowsByPropertyName(key);
				return new ArrayList<>();
			}
		});
		editor.setFreeEdit(true);
		columnFilterEditors.put(columnIndex, editor);
		table.configure();
	}*/

	//==================================================================================================
	
	public int getRowIndexBySelectionPosition(int position){
		return LayerUtil.convertRowPosition(getSelectionLayer(), position, getBodyDataLayer());
	}
	
	public int getRowIndexByTablePosition(int position){
		return LayerUtil.convertRowPosition(table, position, getBodyDataLayer());
	}
	
	public RowData<?> getObjectByTableRowPosition(int position){
		return getBodyDataProvider().getRowObject(getRowIndexByTablePosition(position));
	}
	
	public RowData<?> getObjectBySelectionRowPosition(int position){
		return getBodyDataProvider().getRowObject(getRowIndexBySelectionPosition(position));
	}
	
	public void setDataSelectionListener(final IDataSelectionListener listener){
		table.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event paramEvent) {
				listener.tableSelectionChanged();
			}
		});
	}
	
	public RowData<?> getSelectedObject(){
		RowData<?>[] arr = getSelectedObjects();
		if(arr.length>0) return arr[0];
		return null;
	}
	
	public RowData<?>[] getSelectedObjects(){
		ArrayList<RowData<?>> arr = new ArrayList<>();
		Set<Range> ranges = bodyLayerStack.getSelectionLayer().getSelectedRowPositions();
		for(Iterator<Range> it1 = ranges.iterator(); it1.hasNext();){
			Range range = it1.next();
			for(Iterator<Integer> it2 = range.getMembers().iterator(); it2.hasNext();){
				Integer pos = it2.next();
				arr.add(getObjectBySelectionRowPosition(pos));
			}
		}
		return (RowData<?>[]) arr.toArray(new RowData<?>[0]);
	}
}
