package org.ow.eclipse.rcp.jurt.gui.unitTable.configs;

import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.config.*;
import org.eclipse.nebula.widgets.nattable.grid.GridRegion;
import org.eclipse.nebula.widgets.nattable.layer.cell.ILayerCell;
import org.eclipse.nebula.widgets.nattable.painter.cell.*;
import org.eclipse.nebula.widgets.nattable.painter.cell.decorator.CellPainterDecorator;
import org.eclipse.nebula.widgets.nattable.painter.cell.decorator.PaddingDecorator;
import org.eclipse.nebula.widgets.nattable.painter.layer.NatGridLayerPainter;
import org.eclipse.nebula.widgets.nattable.style.*;
import org.eclipse.nebula.widgets.nattable.ui.util.CellEdgeEnum;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.Display;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.gui.unitTable.UnitTableModel;
import org.ow.eclipse.rcp.jurt.model.wrappers.*;

public class CustomStyleConfiguration extends AbstractRegistryConfiguration {

	private static Image	descriptionImage	= SWTResourceManager.getImage(CustomStyleConfiguration.class, "/resources/icons/info.gif");
	private static Image	parametersImage		= SWTResourceManager.getImage(CustomStyleConfiguration.class, "/resources/icons/params.png");
	private static Image	editableImage		= SWTResourceManager.getImage(CustomStyleConfiguration.class, "/resources/icons/edit.png");

	private NatTable		table;
	private UnitTableModel	tableModel;

	private TextPainter		defaultTextPainter	= new TextPainter(false, true, false, true);
	private Style			boldStyle;

	public CustomStyleConfiguration(NatTable table, UnitTableModel tableModel) {
		super();
		this.table = table;
		this.tableModel = tableModel;
		init();
	}

	private void init() {
		ConfigRegistry configRegistry = (ConfigRegistry) table.getConfigRegistry();

		table.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		table.setLayerPainter(new NatGridLayerPainter(table, table.getBackground()));

		TableImagePainter tableImagePainter = new TableImagePainter(tableModel);
		CellPainterDecorator cellPainterDecorator = new TableCellPainterDecorator(tableModel, defaultTextPainter, CellEdgeEnum.LEFT, tableImagePainter);
		Font boldFont = SWTResourceManager.getBoldFont(Display.getCurrent().getSystemFont());
		boldStyle = new Style();
		boldStyle.setAttributeValue(CellStyleAttributes.FONT, boldFont);

		// стиль текста по умолчанию
		Style defaultStyle = new Style();
		defaultStyle.setAttributeValue(CellStyleAttributes.HORIZONTAL_ALIGNMENT, HorizontalAlignmentEnum.LEFT);
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_STYLE, defaultStyle, DisplayMode.NORMAL, GridRegion.BODY);

		// padding в ячейках по умолчанию
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, new PaddingDecorator(cellPainterDecorator, 0, 6, 0, 6), DisplayMode.NORMAL, GridRegion.BODY);

		// выравнивание по центру для 0 и 2 столбца
		Style centerAlignStyle = new Style();
		centerAlignStyle.setAttributeValue(CellStyleAttributes.HORIZONTAL_ALIGNMENT, HorizontalAlignmentEnum.CENTER);
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_STYLE, centerAlignStyle, DisplayMode.NORMAL, "col_0");
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_STYLE, centerAlignStyle, DisplayMode.NORMAL, "col_2");

		// меньший padding для столбца с деревом
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, new PaddingDecorator(cellPainterDecorator, 0, 6, 0, 0), DisplayMode.NORMAL, "col_1");

		// только изображение для столбца статуса
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, tableImagePainter, DisplayMode.NORMAL, "col_2");
	}

	@Override
	public void configureRegistry(IConfigRegistry configRegistry) {
		int l = tableModel.getBodyDataProvider().getRowCount();
		for (int i = 0; i < l; i++) {
			Object obj = tableModel.getBodyDataProvider().getRowObject(i);
			// жирный шрифт для строк bundle'ов
			configRegistry.unregisterConfigAttribute(CellConfigAttributes.CELL_STYLE, DisplayMode.NORMAL, "row_" + i);
			configRegistry.unregisterConfigAttribute(CellConfigAttributes.CELL_STYLE, DisplayMode.SELECT, "row_" + i);
			if (obj != null && obj instanceof BundleRow) {
				configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_STYLE, boldStyle, DisplayMode.NORMAL, "row_" + i);
				configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_STYLE, boldStyle, DisplayMode.SELECT, "row_" + i);
			}
		}
	}

	//Декоратор для рисования иконок в конце ячейки
	private class TableCellPainterDecorator extends CellPainterDecorator {

		private UnitTableModel tableModel;

		public TableCellPainterDecorator(UnitTableModel tableModel, ICellPainter baseCellPainter, CellEdgeEnum cellEdge, ICellPainter decoratorCellPainter) {
			super(baseCellPainter, cellEdge, decoratorCellPainter);
			this.tableModel = tableModel;
		}

		@Override
		public void paintCell(ILayerCell cell, GC gc, Rectangle adjustedCellBounds, IConfigRegistry configRegistry) {
			Rectangle baseCellPainterBounds = getBaseCellPainterBounds(cell, gc, adjustedCellBounds, configRegistry);
			Rectangle decoratorCellPainterBounds = getDecoratorCellPainterBounds(cell, gc, adjustedCellBounds, configRegistry);

			Color originalBg = gc.getBackground();
			gc.setBackground((Color) CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyleAttributes.BACKGROUND_COLOR));
			gc.fillRectangle(adjustedCellBounds);
			gc.setBackground(originalBg);

			getComplexCellPainter(cell, getBaseCellPainter()).paintCell(cell, gc, baseCellPainterBounds, configRegistry);
			getDecoratorCellPainter().paintCell(cell, gc, decoratorCellPainterBounds, configRegistry);
		}

		@Override
		public ICellPainter getCellPainterAt(int x, int y, ILayerCell cell, GC gc, Rectangle adjustedCellBounds, IConfigRegistry configRegistry) {
			ICellPainter cellPainter = super.getCellPainterAt(x, y, cell, gc, adjustedCellBounds, configRegistry);
			return getComplexCellPainter(cell, cellPainter);
		}

		private ICellPainter getComplexCellPainter(ILayerCell cell, ICellPainter sourceCellPainter) {
			int colPos = cell.getColumnIndex();
			ICellPainter cellPainter = sourceCellPainter;
			if (colPos == 1 && sourceCellPainter.equals(getBaseCellPainter())) {
				int rowPos = cell.getRowPosition();
				RowData<?> row = tableModel.getObjectByTableRowPosition(rowPos);
				if (!row.getDescription().isEmpty()) {
					cellPainter = addImageAtRightPainter(cellPainter, descriptionImage);
				}
				if (row instanceof ClassRow) {
					ClassRow cRow = (ClassRow) row;
					if (cRow.getParameters() != null) {
						cellPainter = addImageAtRightPainter(cellPainter, parametersImage);
						if (cRow.getParameters().isEditable()) {
							cellPainter = addImageAtRightPainter(cellPainter, editableImage);
						}
					}
				}
			}
			return cellPainter;
		}

		private ICellPainter addImageAtRightPainter(ICellPainter cellPainter, Image image) {
			return new CellPainterDecorator(cellPainter, CellEdgeEnum.RIGHT, new ImagePainter(image));
		}
	}

	private class TableImagePainter extends ImagePainter {

		private UnitTableModel tableModel;

		public TableImagePainter(UnitTableModel tableModel) {
			super();
			this.tableModel = tableModel;
		}

		protected Image getImage(ILayerCell cell, IConfigRegistry configRegistry) {
			int colPos = cell.getColumnIndex();
			int rowPos = cell.getRowPosition();
			RowData<?> row = tableModel.getObjectByTableRowPosition(rowPos);
			if (colPos == 1) return row.getObjectIcon();
			if (colPos == 2) return row.getStateIcon();
			return null;
		}
	}

}
