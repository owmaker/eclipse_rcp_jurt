package org.ow.eclipse.rcp.jurt.gui.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.ow.eclipse.rcp.jurt.gui.panels.ViewPanel;
import org.ow.eclipse.rcp.jurt.model.TestsManager;

public class View extends ViewPart {

	private TestsManager testsManager;
	private ViewPanel viewPanel;
	
	@Override
	public void createPartControl(Composite parent) {
		testsManager = new TestsManager(this);
		viewPanel = new ViewPanel(parent, SWT.NONE, this);
	}

	@Override
	public void setFocus() {
	}

	public TestsManager getTestsManager() {
		return testsManager;
	}

	public ViewPanel getViewPanel() {
		return viewPanel;
	}

	public static Shell getShell(){
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}
	
	@Override
	public void dispose() {
		viewPanel.dispose();
        super.dispose();
	}
	
	public static void GC(){
		Thread gc = new Thread(new Runnable() {
			public void run() {
				System.gc();
			}
		});
		gc.start();
	}
	

}
