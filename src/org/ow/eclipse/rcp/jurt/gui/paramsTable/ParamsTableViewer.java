package org.ow.eclipse.rcp.jurt.gui.paramsTable;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.model.ParamsConfig.ParamLine;
import org.ow.eclipse.rcp.jurt.model.ParamsConfig.Parameter;
import org.ow.eclipse.rcp.jurt.model.TestClassParams;

public class ParamsTableViewer extends TableViewer {
	
	private Table table;
	private Listener resizeListener;
	private TableEditor tableEditor;
	private TestClassParams config;
	
	public ParamsTableViewer(Composite parent) {
		super(parent, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		
		table = getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		resizeListener = getTableResizeListener();
		table.addListener(SWT.Resize, resizeListener);
		table.addListener(SWT.MouseDown, new EditOnMouseDownListener());
		
		ColumnViewerToolTipSupport.enableFor(this);
		
		tableEditor = new TableEditor(table);
		tableEditor.horizontalAlignment = SWT.LEFT;
		tableEditor.grabHorizontal = true;
	}

	public boolean isEditable(){
		return config != null && config.isEditable();
	}
	
	//------------------------------
	
	public void setInput(TestClassParams testClassParams){
		stopEditing();
		config = testClassParams;
		
		table.setRedraw(false);
		if(config!=null){
			initColumns(config.getParamTitles());
			initRows(config.getParams().getParamLines());
		} else {
			initColumns(null);
			initRows(null);
		}
		table.setRedraw(true);
	}
	
	private void stopEditing(){
		if(tableEditor.getEditor() != null){
			tableEditor.getEditor().dispose();
		}
	}
	
	private void initColumns(ArrayList<String> titles){
		int l = table.getColumnCount();
		for(int i = l-1 ; i >= 0 ; i--){
			table.getColumn(i).dispose();
		}
		if(titles==null) return;
		TableColumn indxColumn = new TableColumn(table, SWT.NONE);
		indxColumn.setText("#");
		for(String title : titles){
			TableColumn tc = new TableColumn(table, SWT.NONE);
			tc.setText(title);
			tc.setToolTipText(title);
		}
	}
	
	private void initRows(ArrayList<ParamLine> paramLines){
		int l = table.getItemCount();
		for(int i = l-1 ; i >= 0 ; i--){
			TableItem item = table.getItem(i);
			table.remove(i);
			item.dispose();
		}
		if(paramLines==null) return;
		int i = 0;
		for(ParamLine paramLine : paramLines){
			TableItem item = new TableItem(table, SWT.NONE);
			ArrayList<String> strs = new ArrayList<>();
			strs.add(String.valueOf(i) + ".");
			for (Parameter param : paramLine.getParams()) {
				strs.add(param.getValue());
			}
			item.setText(strs.toArray(new String[0]));
			i++;
		}
		table.notifyListeners(SWT.Resize, new Event());
	}
	
	//------------------------------
	
	public void addRow(){
		ParamLine paramLine = new ParamLine();
		for(int i = 0 ; i < table.getColumnCount()-1 ; i++){
			paramLine.addParam(new Parameter(""));
		}
		config.getParams().addParamLine(paramLine);
		setInput(config);
	}
	
	public void removeRow(){
		int[] selectedRows = table.getSelectionIndices();
		for(int selectedRow : selectedRows){
			config.getParams().removeParamLine(selectedRow);
		}
		setInput(config);
	}

	//------------------------------
	
	private void fitColumnsWidth(){
		table.removeListener(SWT.Resize, resizeListener);
		Rectangle rect = table.getClientArea();
		int l = table.getColumnCount();
		if(rect.width > 0 && l > 0){
			table.getColumn(0).pack();
			int width = (rect.width - table.getColumn(0).getWidth())/(l - 1);
			for(int i = l-1 ; i >= 1 ; i--){
				table.getColumn(i).setWidth(width);
			}
		}
		table.addListener(SWT.Resize, resizeListener);
	}
	
	private Listener getTableResizeListener(){
		return new Listener(){
			@Override
			public void handleEvent(Event e) {
				Point cp = e.display.getCursorLocation();
				Rectangle tr = table.getClientArea();
				Point tp = table.toDisplay(new Point(0, 0));
				tr.x = tp.x;
				tr.y = tp.y;
				if(!tr.contains(cp)){
					fitColumnsWidth();
				}
			}
		};
	}
	
	private class EditOnMouseDownListener implements Listener{

		@Override
		public void handleEvent(Event e) {
			
			final Table table = ParamsTableViewer.this.table;
			final TableEditor tableEditor = ParamsTableViewer.this.tableEditor;
			final TestClassParams config = ParamsTableViewer.this.config;

			if(config == null || !config.isEditable()) return;
			
			Control oldCellEditor = tableEditor.getEditor();
			if (oldCellEditor != null) oldCellEditor.dispose();

			Point pt = new Point(e.x, e.y);
			TableItem item = table.getItem(pt);
			if (item == null) return;
			int k = -1;
			for (int i = 0, count = table.getColumnCount(); i < count; i++) {
				if (item.getBounds(i).contains(pt)) k = i;
			}
			if(k == 0) return;
			final int row = Arrays.asList(table.getItems()).indexOf(item);
			final int col = k;

			Text newCellEditor = new Text(table, SWT.MULTI);
			newCellEditor.setText(item.getText(col));
			newCellEditor.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent arg0) {
					Text text = (Text) tableEditor.getEditor();
					String newValue = text.getText();
					TableItem item = tableEditor.getItem();
					item.setText(col, newValue);
					config.getParams().getParamLine(row).getParam(col - 1).setValue(newValue);
				}
			});
			newCellEditor.selectAll();
			newCellEditor.setFocus();
			tableEditor.setEditor(newCellEditor, item, col);
		}
	}
}
