package org.ow.eclipse.rcp.jurt.gui.panels;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.*;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.gui.unitTable.UnitTableModel;
import org.ow.eclipse.rcp.jurt.gui.view.View;
import org.ow.eclipse.rcp.jurt.model.TestClassParams;
import org.ow.eclipse.rcp.jurt.model.wrappers.*;

public class ViewPanel extends Composite implements IDataSelectionListener {

	private View					view;
	private Text					runsText;
	private Text					errorsText;
	private Text					failuresText;

	private Button					findButton;
	private Button					runButton;

	private ProgressBar				progressBar;

	private StyledText				unitDescription;
	private ParamsPanel				paramsPanel;
	private StyledText				failureDescription;

	private NatTable				unitTable;
	private UnitTableModel			unitTableModel;
	private ArrayList<RowData<?>>	tableRows	= new ArrayList<>();

	public ViewPanel(Composite parent, int style, View view) {
		super(parent, style);
		this.view = view;
		init();
	}

	private void init() {

		setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		GridLayout gl_panel = new GridLayout();
		setLayout(gl_panel);

		// ---------------------------------------------------------------------

		Composite headPanel = new Composite(this, SWT.NONE);
		GridLayout gl_headPanel = new GridLayout(6, false);
		gl_headPanel.horizontalSpacing = 8;
		gl_headPanel.verticalSpacing = 0;
		gl_headPanel.marginWidth = 0;
		gl_headPanel.marginHeight = 0;
		headPanel.setLayout(gl_headPanel);
		headPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		findButton = new Button(headPanel, SWT.NONE);
		findButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		findButton.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/refresh_nav.gif"));
		findButton.setText("Find");
		findButton.addSelectionListener(getFindSelectionAdapter());

		Label separator_1 = new Label(headPanel, SWT.SEPARATOR);
		GridData gd_separator_1 = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		separator_1.setLayoutData(gd_separator_1);

		runButton = new Button(headPanel, SWT.NONE);
		runButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		runButton.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/run_exc.gif"));
		runButton.setText("Run");
		runButton.addSelectionListener(getRunSelectionAdapter());

		Composite indicatorPanel = new Composite(headPanel, SWT.NONE);
		GridLayout gl_indicatorPanel = new GridLayout(6, false);
		gl_indicatorPanel.marginWidth = gl_indicatorPanel.marginHeight = 0;
		indicatorPanel.setLayout(gl_indicatorPanel);
		indicatorPanel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Label runsLabel = new Label(indicatorPanel, SWT.NONE);
		runsLabel.setText("Runs:");

		runsText = new Text(indicatorPanel, SWT.NONE);
		runsText.setEditable(false);
		runsText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridData gd_runsText = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_runsText.widthHint = 60;
		runsText.setLayoutData(gd_runsText);
		runsText.setText("0");

		CLabel errorsLabel = new CLabel(indicatorPanel, SWT.NONE);
		errorsLabel.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/state_ovr_error.gif"));
		GridData gd_errorsLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gd_errorsLabel.horizontalIndent = 6;
		errorsLabel.setLayoutData(gd_errorsLabel);
		errorsLabel.setText("Errors:");

		errorsText = new Text(indicatorPanel, SWT.NONE);
		errorsText.setEditable(false);
		errorsText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridData gd_errorsText = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_errorsText.widthHint = 60;
		errorsText.setLayoutData(gd_errorsText);
		errorsText.setText("0");

		CLabel failuresLabel = new CLabel(indicatorPanel, SWT.NONE);
		failuresLabel.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/state_ovr_failed.gif"));
		GridData gd_lblNewLabel_3 = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		gd_lblNewLabel_3.horizontalIndent = 6;
		failuresLabel.setLayoutData(gd_lblNewLabel_3);
		failuresLabel.setText("Failures:");

		failuresText = new Text(indicatorPanel, SWT.NONE);
		failuresText.setEditable(false);
		failuresText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridData gd_failuresText = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_failuresText.widthHint = 60;
		failuresText.setLayoutData(gd_failuresText);
		failuresText.setText("0");

		Label separator_2 = new Label(headPanel, SWT.SEPARATOR);
		GridData gd_separator_2 = new GridData(SWT.RIGHT, SWT.CENTER, true, false);
		separator_2.setLayoutData(gd_separator_2);

		progressBar = new ProgressBar(headPanel, SWT.NONE);
		GridData gd_progressBar = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_progressBar.widthHint = 200;
		progressBar.setLayoutData(gd_progressBar);

		// ---------------------------------------------------------------------

		SashForm sashForm = new SashForm(this, SWT.NONE);
		sashForm.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridData gd_sashForm = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_sashForm.widthHint = 600;
		gd_sashForm.heightHint = 400;
		sashForm.setLayoutData(gd_sashForm);

		Composite testsTreePanel = new Composite(sashForm, SWT.NONE);
		// testsTreePanel.setBackgroundMode(SWT.INHERIT_FORCE);
		// testsTreePanel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		testsTreePanel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		testsTreePanel.setLayout(new GridLayout());

		Composite composite_1 = new Composite(testsTreePanel, SWT.NONE);
		GridLayout gl_composite_1 = new GridLayout();
		gl_composite_1.marginWidth = 0;
		gl_composite_1.marginHeight = 0;
		composite_1.setLayout(gl_composite_1);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		CLabel lblNewLabel = new CLabel(composite_1, SWT.NONE);
		lblNewLabel.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/hierarchical.gif"));
		lblNewLabel.setText("Test units:");

		unitTable = initUnitTable(testsTreePanel);
		unitTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Composite descriptionPanel = new Composite(sashForm, SWT.NONE);
		// descriptionPanel.setBackgroundMode(SWT.INHERIT_FORCE);
		// descriptionPanel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		descriptionPanel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		descriptionPanel.setLayout(new GridLayout());

		Composite composite_2 = new Composite(descriptionPanel, SWT.NONE);
		GridLayout gl_composite_2 = new GridLayout();
		gl_composite_2.marginHeight = 0;
		gl_composite_2.marginWidth = 0;
		composite_2.setLayout(gl_composite_2);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		CLabel lblNewLabel_1 = new CLabel(composite_2, SWT.NONE);
		lblNewLabel_1.setText("Descriptions:");

		initDescriptionTabFolder(descriptionPanel);

		sashForm.setWeights(new int[] {20, 9});

		// ---------------------------------------------------------------------

		pack();
		gd_separator_1.heightHint = gd_separator_2.heightHint = progressBar.getSize().y;
		pack();
	}

	private void initDescriptionTabFolder(Composite parent) {

		CTabFolder tabFolder = new CTabFolder(parent, SWT.BORDER);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		unitDescription = new StyledText(tabFolder, SWT.READ_ONLY | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.WRAP);
		unitDescription.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		unitDescription.setAlwaysShowScrollBars(false);

		CTabItem unitDescrItem = new CTabItem(tabFolder, SWT.NONE);
		unitDescrItem.setText("   Unit   ");
		unitDescrItem.setImage(SWTResourceManager.getImage(MethodRow.class, "/resources/icons/info.gif"));
		unitDescrItem.setControl(unitDescription);

		// ------

		paramsPanel = new ParamsPanel(tabFolder, SWT.NONE, view);

		CTabItem paramsDescrItem = new CTabItem(tabFolder, SWT.NONE);
		paramsDescrItem.setText("  Params  ");
		paramsDescrItem.setImage(SWTResourceManager.getImage(MethodRow.class, "/resources/icons/params.png"));
		paramsDescrItem.setControl(paramsPanel);

		// ------

		failureDescription = new StyledText(tabFolder, SWT.READ_ONLY | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		failureDescription.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		failureDescription.setAlwaysShowScrollBars(false);

		CTabItem failureDescrItem = new CTabItem(tabFolder, SWT.NONE);
		failureDescrItem.setText("  Failure  ");
		failureDescrItem.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/stackframe.gif"));
		failureDescrItem.setControl(failureDescription);

		tabFolder.setSelection(unitDescrItem);

	}

	private NatTable initUnitTable(Composite parent) {
		Integer[] columnWidths = {30, 450, 50, 110};

		NatTable table = new NatTable(parent, NatTable.DEFAULT_STYLE_OPTIONS | SWT.BORDER, false);
		unitTableModel = new UnitTableModel(view, table, tableRows, false);

		unitTableModel.setColumnWidths(columnWidths);

		unitTableModel.setDataSelectionListener(this);
		unitTableModel.setFiltersVisible(false);

		return table;
	}

	// -----------------------------------------------

	private SelectionAdapter getFindSelectionAdapter() {
		return new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				findTests();
			}
		};
	}

	private SelectionAdapter getRunSelectionAdapter() {
		return new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				runTests();
			}
		};
	}

	// -----------------------------------------------

	private void findTests() {
		Job findTests = new Job("Tests search") {

			@Override
			protected IStatus run(IProgressMonitor mon) {
				enableButtons(false);
				setProgressBar(SWT.INDETERMINATE);
				setProgressbarState(SWT.NORMAL);

				view.getTestsManager().clear();
				refreshSummInfo();
				refreshTable();
				view.getTestsManager().findTests();
				refreshTable();

				setProgressBar(SWT.NONE);
				enableButtons(true);
				return Status.OK_STATUS;
			}
		};
		findTests.schedule();
	}

	private void runTests() {
		Job findTests = new Job("Tests execution") {

			@Override
			protected IStatus run(IProgressMonitor mon) {
				enableButtons(false);

				setProgressbarValue(0, 100);
				view.getTestsManager().clearSummInfo();
				refreshSummInfo();
				view.getTestsManager().runTests();
				refreshSummInfo();

				refreshTable();
				enableButtons(true);
				return Status.OK_STATUS;
			}
		};
		findTests.schedule();
	}

	// -----------------------------------------------

	private void enableButtons(final boolean enable) {
		View.getShell().getDisplay().syncExec(new Runnable() {

			public void run() {
				findButton.setEnabled(enable);
				runButton.setEnabled(enable);
			}
		});
	}

	private void setProgressBar(final int style) {
		if (!isDisposed()) {
			View.getShell().getDisplay().syncExec(new Runnable() {

				public void run() {
					Composite parent = progressBar.getParent();
					Object layoutData = progressBar.getLayoutData();
					progressBar.dispose();
					progressBar = new ProgressBar(parent, style);
					progressBar.setLayoutData(layoutData);
					progressBar.getParent().layout(true, true);
				}
			});
		}
	}

	private void refreshSummInfo() {
		if (!isDisposed()) {
			View.getShell().getDisplay().syncExec(new Runnable() {

				public void run() {
					failureDescription.setText("");
					runsText.setText(String.valueOf(view.getTestsManager().getLastRunTestsCount() + "/" + view.getTestsManager().getLastSummTestsCount()));
					errorsText.setText(String.valueOf(view.getTestsManager().getLastErrorsCount()));
					failuresText.setText(String.valueOf(view.getTestsManager().getLastFailuresCount()));
					
					int state = SWT.NORMAL;
					if(view.getTestsManager().getLastErrorsCount() != 0) state = SWT.ERROR;
					else if(view.getTestsManager().getLastFailuresCount() != 0) state = SWT.PAUSED;
					setProgressbarState(state);
				}
			});
		}
	}

	public void refreshTable() {
		if (!isDisposed()) {
			View.getShell().getDisplay().syncExec(new Runnable() {

				public void run() {
					setTableRows(view.getTestsManager().getTableRows());
				}
			});
		}
	}

	private void setTableRows(List<RowData<?>> list) {
		tableRows.clear();
		tableRows.addAll(list);
		unitTableModel.refreshTable();
	}

	public void redrawTable() {
		if (!isDisposed()) {
			View.getShell().getDisplay().syncExec(new Runnable() {

				public void run() {
					unitTableModel.redrawTable();
				}
			});
		}
	}

	// -----------------------------------------------

	@Override
	public void tableSelectionChanged() {
		View.getShell().getDisplay().asyncExec(new Runnable() {

			public void run() {
				RowData<?> row = unitTableModel.getSelectedObject();
				String unitDescr = "";
				TestClassParams testClassParams = null;
				String failureDescr = "";
				if (row != null) {
					unitDescr = row.getDescription();
					if (row instanceof ClassRow) {
						testClassParams = ((ClassRow) row).getParameters();
					}
					failureDescr = row.getLastResDescription();
				}
				unitDescription.setText(unitDescr);
				paramsPanel.setInput(testClassParams);
				failureDescription.setText(failureDescr);
				if (row instanceof BundleRow) applyBundleDescriptionStyling();
			}
		});
	}

	private void applyBundleDescriptionStyling() {
		unitDescription.setStyleRange(null);
		ArrayList<StyleRange> srs = new ArrayList<>();
		int k1 = 0;
		while (unitDescription.getText().indexOf(":", k1) != -1) {
			int k2 = unitDescription.getText().indexOf(":", k1);
			if (unitDescription.getText().indexOf("\n", k1 + 1) > k2) {
				StyleRange sr = new StyleRange();
				sr.start = k1;
				sr.length = k2 - k1 + 1;
				sr.fontStyle = SWT.BOLD;
				srs.add(sr);
				k1 = unitDescription.getText().indexOf("\n", k2);
			} else {
				k1 = unitDescription.getText().indexOf("\n", k1 + 1);
			}
		}
		unitDescription.setStyleRanges(srs.toArray(new StyleRange[0]));
	}
	
	private void setProgressbarState(final int state) {
		if (!isDisposed()) {
			getDisplay().syncExec(new Runnable() {

				public void run() {
					progressBar.setState(state);
				}
			});
		}
	}

	public void setProgressbarValue(final int current, final int total) {
		if (!isDisposed()) {
			getDisplay().syncExec(new Runnable() {

				public void run() {
					if (!progressBar.isDisposed()) {
						progressBar.setMinimum(0);
						progressBar.setMaximum(total);
						progressBar.setSelection(current);
						progressBar.redraw();
					}
				}
			});
			try {
				Thread.sleep(333);
			} catch (InterruptedException e) {
				// e.printStackTrace();
			}
		}
	}
	
	public RowData<?> getSelectedRow() {
		return unitTableModel.getSelectedObject();
	}
}
