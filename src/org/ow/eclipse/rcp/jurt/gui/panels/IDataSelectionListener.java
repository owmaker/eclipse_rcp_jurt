package org.ow.eclipse.rcp.jurt.gui.panels;

public interface IDataSelectionListener {

	public void tableSelectionChanged();
	
}
