package org.ow.eclipse.rcp.jurt.gui.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.ow.eclipse.rcp.jurt.gui.SWTResourceManager;
import org.ow.eclipse.rcp.jurt.gui.paramsTable.ParamsTableViewer;
import org.ow.eclipse.rcp.jurt.gui.view.View;
import org.ow.eclipse.rcp.jurt.model.TestClassParams;
import org.ow.eclipse.rcp.jurt.model.wrappers.ClassRow;
import org.ow.eclipse.rcp.jurt.model.wrappers.RowData;

public class ParamsPanel extends Composite {

	private View				view;
	
	private ParamsTableViewer	paramsTableViewer;

	private Button				addParamLineButton;
	private Button				removeParamLineButton;
	private Button				loadConfig;
	private Button				saveClassConfigButton;
	private Button				saveAllConfigs;

	public ParamsPanel(Composite parent, int style, View view) {
		super(parent, style);
		this.view = view;
		init();
	}

	private void init() {

		setBackgroundMode(SWT.INHERIT_DEFAULT);
		GridLayout gl_paramsPanel = new GridLayout();
		gl_paramsPanel.marginHeight = gl_paramsPanel.marginWidth = gl_paramsPanel.verticalSpacing = 0;
		setLayout(gl_paramsPanel);

		paramsTableViewer = initParamsTable(this);
		paramsTableViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Composite paramCommandsPanel = new Composite(this, SWT.NONE);
		paramCommandsPanel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		GridLayout gl_paramCommandsPanel = new GridLayout(6, false);
		gl_paramCommandsPanel.marginWidth = gl_paramCommandsPanel.marginHeight = 3;
		paramCommandsPanel.setLayout(gl_paramCommandsPanel);

		addParamLineButton = new Button(paramCommandsPanel, SWT.NONE);
		addParamLineButton.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/add.png"));
		addParamLineButton.setToolTipText("Add line");
		addParamLineButton.setEnabled(false);
		addParamLineButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addParamLineButton.addSelectionListener(getAddParamLineSelectionAdapter());

		removeParamLineButton = new Button(paramCommandsPanel, SWT.NONE);
		removeParamLineButton.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/remove.png"));
		removeParamLineButton.setToolTipText("Remove line");
		removeParamLineButton.setEnabled(false);
		removeParamLineButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		removeParamLineButton.addSelectionListener(getRemoveParamLineSelectionAdapter());

		Label lbl = new Label(paramCommandsPanel, SWT.NONE);
		lbl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		loadConfig = new Button(paramCommandsPanel, SWT.NONE);
		loadConfig.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/open.png"));
		loadConfig.setToolTipText("Load params");
		loadConfig.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		loadConfig.addSelectionListener(getLoadSelectionAdapter());

		saveClassConfigButton = new Button(paramCommandsPanel, SWT.NONE);
		saveClassConfigButton.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/save.png"));
		saveClassConfigButton.setToolTipText("Save class params");
		saveClassConfigButton.setEnabled(false);
		saveClassConfigButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		saveClassConfigButton.addSelectionListener(getSaveClassSelectionAdapter());

		saveAllConfigs = new Button(paramCommandsPanel, SWT.NONE);
		saveAllConfigs.setImage(SWTResourceManager.getImage(ViewPanel.class, "/resources/icons/saveall.png"));
		saveAllConfigs.setToolTipText("Save all params");
		saveAllConfigs.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		saveAllConfigs.addSelectionListener(getSaveAllSelectionAdapter());
	}

	private ParamsTableViewer initParamsTable(Composite parent) {
		final ParamsTableViewer tableViewer = new ParamsTableViewer(parent);
		final Table table = tableViewer.getTable();
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeParamLineButton.setEnabled(table.getSelectionCount() != 0 && tableViewer.isEditable());
			}
		});
		
		return tableViewer;
	}

	// -----------------------------------------------

	private SelectionAdapter getAddParamLineSelectionAdapter() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				paramsTableViewer.addRow();
			}
		};
	}

	private SelectionAdapter getRemoveParamLineSelectionAdapter() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				paramsTableViewer.removeRow();
			}
		};
	}

	private SelectionAdapter getLoadSelectionAdapter() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				view.getViewPanel().refreshTable();
				view.getTestsManager().loadConfig();
			}
		};
	}

	private SelectionAdapter getSaveClassSelectionAdapter() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				RowData<?> row = view.getViewPanel().getSelectedRow();
				if(row instanceof ClassRow) {
					view.getTestsManager().saveConfigForClass((ClassRow) row);
				}
			}
		};
	}

	private SelectionAdapter getSaveAllSelectionAdapter() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				view.getTestsManager().saveConfigForAll();
			}
		};
	}

	// -----------------------------------------------
	
	public void setInput(TestClassParams testClassParams){
		paramsTableViewer.setInput(testClassParams);
		addParamLineButton.setEnabled(paramsTableViewer.isEditable());
		removeParamLineButton.setEnabled(false);
		saveClassConfigButton.setEnabled(testClassParams != null);
	}

}
